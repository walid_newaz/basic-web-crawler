import requests
from bs4 import BeautifulSoup
import ssl
import re
import json
import sqlite3
from schema.tables import create_tables


class Spider:
    # Class properties
    config = dict()
    base_url = ""
    start_url = ""
    headers = ""
    db = ""
    ctx = None
    conn = None
    cur = None

    def load_config(self):
        # Load config file
        fhandle = open("config.json")
        self.config = json.loads(fhandle.read())
        self.base_url = self.config['base_url']
        self.start_url = self.config['start_url']
        self.headers = self.config['request_headers']
        self.db = self.config['db']

    def init_net(self):
        # Ignore SSL certificate errors
        self.ctx = ssl.create_default_context()
        self.ctx.check_hostname = False
        self.ctx.verify_mode = ssl.CERT_NONE

    def init_db(self):
        # Connect to Database
        self.conn = sqlite3.connect(self.db)
        self.cur = self.conn.cursor()
        create_tables(self.cur)

    def get_next_url(self):
        print('Retrieving', self.start_url)
        r = requests.get(self.start_url, headers=self.headers)
        html = r.text
        soup = BeautifulSoup(html, "html.parser")
        # todo place in package parsers under mountainproject_com
        name = soup.find("span", itemprop="itemreviewed")
        print(name.string.strip())
        route_rating = soup.find_all("span", class_="rateYDS")
        yds = route_rating[0].text.strip()
        print(yds)

        original_yds = route_rating[1].text.strip()
        print(original_yds)

        avg_rating = soup.find("meta", itemprop="average")
        print(avg_rating["content"].strip())

        votes = soup.find("meta", itemprop="votes")
        print(votes["content"].strip())

        all_tds = soup.find_all("td")
        for td in all_tds:
            if td.find(string=re.compile("\A(?:Trad|Sport|Boulder)")):
                type_pitch = td.text.strip()
                print(type_pitch)

        fa = soup.find(string=re.compile("\AFA:")).parent.find_next("td").text.strip()
        print(fa)

        views = soup.find(string=re.compile("\APage Views:")).parent.find_next("td").text.strip()
        print(views)

        description = soup.find(string=re.compile("\ADescription")).parent.find_next("div").text.strip()
        print(description)

        protection = soup.find(string=re.compile("\AProtection")).parent.find_next("div").text.strip()
        print(protection)

        page_type = soup.find(string=re.compile("templateType"))
        template_type = re.findall("templateType: '([^']*)'", page_type)[0]
        print(template_type)

    '''
    Spider class constructor
    '''
    def __init__(self):
        self.load_config()
        self.init_net()
        self.init_db()


if "__main__" == __name__:
    print('Running spider...')
    spider = Spider()
    spider.get_next_url()